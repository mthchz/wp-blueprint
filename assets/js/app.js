/**
 * App.js
 */

// Main
(function($) {
    'use strict';

    //...

    /* Element is visible ?
     *******************************************************************************/
    $.fn.isVisible = function(partial) {

        if (typeof $(this).offset() === 'undefined') return false;

        var $t = $(this),
            $w = $(window),
            viewTop = $w.scrollTop(),
            viewBottom = viewTop + $w.height() - 100,
            _top = $t.offset().top,
            _bottom = _top + $t.height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);
