<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tailwindcss
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer bg-gray-800 text-white py-24 mt-24">
		<div class="site-info container mx-auto">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'tailwindcss-text' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'tailwindcss-text' ), 'WordPress' );
				?>
			</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
