<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TailwindCSS_starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('w-1/3 px-4'); ?>>

    <div class="flex flex-col h-full bg-white">
        <?php the_post_thumbnail('post-thumb', array( 'class' => 'w-full' )); ?>

        <div class="post-content p-4 h-full">
            <div class="post-meta">
                <?php tailwindcss_posted_on(); ?> /
                <?php $categories_list = get_the_category_list( esc_html__( ', ', 'tailwindcss-text' ) );
                if ( $categories_list ) {
                    printf( '<span class="cat-links">%1$s</span>', $categories_list );
                } ?>
            </div>

            <h3 class="entry-title text-xl font-normal mt-2 mb-8">
                <a class="text-gray-700" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
            </h3>

        </div>
        <div class="text-right p-4">
            <a class="text-yellow-500" href="<?php echo get_permalink(); ?>">+ Lire la suite</a>
        </div>
    </div>


</article><!-- #post-<?php the_ID(); ?> -->
