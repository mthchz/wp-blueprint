<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tailwindcss
 */

$hero_src = (has_post_thumbnail()) ? get_the_post_thumbnail_url() : get_header_image();

get_header();
?>

<section id="primary" class="content-area container mx-auto">
    <div id="main" class="site-main home-site-main">
        <?php
            while ( have_posts() ): the_post();
                the_content();
            endwhile; // End of the loop.
        ?>
    </div><!-- #main -->

</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
