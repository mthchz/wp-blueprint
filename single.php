<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package tailwindcss
 */

get_header();
?>

	<div id="primary" class="content-area container mx-auto">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class('w-1/3'); ?>>
                <header class="entry-header">
                    <?php

                    the_title( '<h1 class="entry-title">', '</h1>' );

                    if ( 'post' === get_post_type() ) :
                        ?>
                        <div class="entry-meta">
                            <?php
                            tailwindcss_posted_on();
                            tailwindcss_posted_by();
                            $categories_list = get_the_category_list(esc_html__(', ', 'tailwindcss-text'));
                            if($categories_list) {
                                printf('<span class="cat-links">%1$s</span>', $categories_list);
                            }
                            ?>
                        </div><!-- .entry-meta -->
                    <?php endif; ?>
                </header><!-- .entry-header -->

                <?php tailwindcss_post_thumbnail(); ?>

                <div class="entry-content">
                    <?php the_content(); ?>
                </div><!-- .entry-content -->


            </article><!-- #post-<?php the_ID(); ?> -->

            <?php
			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
//			if ( comments_open() || get_comments_number() ) :
//				comments_template();
//			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
